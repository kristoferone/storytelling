var config = {
    style: 'mapbox://styles/kristoferone/ck2ou7c8j11k41cnwy8826az5',
    accessToken: 'pk.eyJ1Ijoia3Jpc3RvZmVyb25lIiwiYSI6ImNrMHNrdGZ3dDAzcWIzYnByc2wxeXpxbHoifQ.EZ7q2p1_ggoMksNIQVbNUg',
    showMarkers: true,
    theme: 'light',
    alignment: 'left',
    title: 'The Title Text of this Story',
    subtitle: 'A descriptive and interesting subtitle to draw in the reader',
    byline: 'By a Digital Storyteller',
    footer: 'Source: source citations, etc.',
    chapters: [
        {
            id: 'slug-style-id',
            title: 'Display Title',
            image: '',
            description: 'description',
            location: {
              center: [-75.67386, 45.42231],
              zoom: 6.98,
              pitch: 0.00,
              bearing: 0.00
            },
            onChapterEnter: [
                // {
                //     layer: 'layer-name',
                //     opacity: 1
                // }
            ],
            onChapterExit: [
                // {
                //     layer: 'hillshade_highlight_bright',
                //     opacity: 1.5
                // }
            ]
        },
        {
            id: 'other-identifier',
            title: 'Second Title',
            image: './path/to/image/source.png',
            description: 'Copy these sections to add to your story.',
            location: {
              center: [8.68529, 50.11071],
              zoom: 11.80,
              pitch: 58.50,
              bearing: 0.26
            },
            onChapterEnter: [],
            onChapterExit: []
        }
    ]
};
